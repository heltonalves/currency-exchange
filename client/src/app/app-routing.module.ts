import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '@core/guards';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: '@client/modules/auth/auth.module#AuthModule',
  },
  {
    path: 'currency',
    loadChildren: '@client/modules/currency/currency.module#CurrencyModule',
    canActivate: [ AuthGuard ],
  },
  {
    path: '',
    redirectTo: 'currency',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
