import { Component } from '@angular/core';

import { UserService, CurrencyService } from '@core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  constructor (
    private currencyService: CurrencyService,
  ) {
    this.currencyService.loadCurrencies();
  }

}
