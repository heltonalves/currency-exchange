import { Component } from '@angular/core';

import { MessageService } from '@core/services';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  message: string;

  constructor(
    private messageService: MessageService,
  ) {
    this.messageService.getMessage().subscribe(message => { this.message = message; });
  }

  removeMessage() {
    this.message = '';
  }

}
