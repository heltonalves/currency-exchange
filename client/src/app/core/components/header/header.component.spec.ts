import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@client/shared/shared.module';
import { CoreModule } from '@core/core.module';
import { of } from 'rxjs';

import { HeaderComponent } from './header.component';

import { AuthService } from '@client/core/services';

export class MockAuthService {
  isLoggedCheck$ = of(true);
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let mockAuthService = new MockAuthService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        SharedModule,
      ],
      declarations: [ HeaderComponent ],
      providers: [
        { provide: AuthService, useValue: mockAuthService },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(mockAuthService, 'isLoggedCheck$').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
