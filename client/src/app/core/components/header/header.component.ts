import { Component } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from '@client/core/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  menuAvailable: Subscription;

  constructor(
    private authService: AuthService,
  ) {
    this.menuAvailable = this.authService.isLoggedCheck$.subscribe (
      resp => { this.menuAvailable = resp; });
  }

  logout (): void {
    this.authService.logout();
  }
}
