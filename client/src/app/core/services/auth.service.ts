import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Md5 } from 'ts-md5/dist/md5';

import { UserService } from './user.service';
import { User, CurrentUser } from '@core/models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  localStorageKey = 'current-user';

  private isLoggedCheck = new Subject<any>();
  public isLoggedCheck$ = this.isLoggedCheck.asObservable();

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
  ) { }

  public createCurrentUser(user: User): void {
    const currentUser: CurrentUser = {
      user: user,
      token: `${Md5.hashStr(`${user.username}-${user.password}`)}`,
    };

    localStorage.setItem(this.localStorageKey, JSON.stringify(currentUser));
  }

  public login (username: string, password: string): Observable<User> {
    return this.userService.get(username)
      .pipe(
        map((user: User) => {
          if (user && user.password === password) {
            this.createCurrentUser(user);
            this.isLoggedCheck.next(true);
            return user;
          }
        }),
      );
  }

  logout() {
    localStorage.removeItem(this.localStorageKey);
    this.isLoggedCheck.next(false);
    this.router.navigate(['/login']);
  }

  public getCurrentUser(): CurrentUser {
    return JSON.parse(localStorage.getItem(this.localStorageKey)) || {};
  }

  public isLogged(): boolean {
    if (this.getCurrentUser().token) { return true; }
    return false;
  }
}
