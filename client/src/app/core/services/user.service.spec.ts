import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from '@core/models';

describe('UserService', () => {
  let service: UserService;
  let httpTestingController: HttpTestingController;

  const mockUsers: User[] = [
    { username: 'test1', password: 'test1', fullName: 'test1' },
    { username: 'test2', password: 'test2', fullName: 'test2' },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ UserService ],
    });

    service = TestBed.get(UserService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return user list', () => {
    service.list()
    .subscribe((users: User[]) => {
      expect(users).toBe(mockUsers);
      expect(users.length).toBe(2);
    });

    const req = httpTestingController.expectOne(`/users`);
    expect(req.request.method).toBe('GET');
  });

  it('should return user object', () => {
    const mockUser: User = mockUsers[0];

    service.get('test1')
    .subscribe((user: User) => {
      expect(user).toBe(mockUser);
    });

    const req = httpTestingController.expectOne(`/users`);
    expect(req.request.method).toBe('GET');
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
