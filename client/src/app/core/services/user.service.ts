import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '@core/models';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  public list(): Observable<User[]> {
    const url = `/users`;
    return this.http.get<User[]>('/users');
  }

  public get(username: string): Observable<User> {
    const url = `/users`;
    return this.list().pipe(
      map((resp: User[]) => {
        return resp.filter((user: User) => user.username === username)[0];
      }),
    );
  }
}
