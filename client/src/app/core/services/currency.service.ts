import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';
import { CurrencyRate, CurrencyConverter, ExchangeCurrency } from '@core/models';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {

  baseUrl = 'https://api.nomics.com/v1/';
  localStorageKey = 'nomics-currencies-rates';
  localStorageHistory = 'currency-history';

  constructor(
    private http: HttpClient,
  ) { }

  private setParams(listParams?: {}): object {
    let params = new HttpParams({ fromObject: listParams });
    params = params.append('key', environment.nomicsToken);
    return { params };
  }

  public currenciesRatesInUSD(): Observable<CurrencyRate[]> {
    const url = `${this.baseUrl}exchange-rates`;
    return this.http.get(url, this.setParams()).pipe(
      map((resp: CurrencyRate[]) => {
        localStorage.setItem(this.localStorageKey, JSON.stringify(resp));
        return resp;
      }),
    );
  }

  public exchangeRatesHistory(currency: string, days: number): Observable<ExchangeCurrency[]> {
    const url = `${this.baseUrl}exchange-rates/history`;

    const dateStart = new Date();
    dateStart.setDate(dateStart.getDate() - days);
    const start: string = dateStart.toISOString();
    const end: string = new Date().toISOString();

    return this.http.get<ExchangeCurrency[]>(url, this.setParams({ currency, end, start }));
  }

  public currenciesRate(): Observable<CurrencyRate[]> {
    const currencies: CurrencyRate[] = JSON.parse(localStorage.getItem(this.localStorageKey));
    if (currencies) {
      return of(currencies);
    }

    return this.currenciesRatesInUSD();
  }

  public loadCurrencies(): void {
    this.currenciesRatesInUSD().subscribe();
  }

  public getCurrencyRate(currency: string): CurrencyRate {
    const currencies: CurrencyRate[] = JSON.parse(localStorage.getItem(this.localStorageKey));
    return currencies.find((item: CurrencyRate) => item.currency === currency );
  }

  public getHistories(): CurrencyConverter[] {
    const histories: CurrencyConverter[] = JSON.parse(localStorage.getItem(this.localStorageHistory)) || [];
    return this.sortByCreatedAt(histories);
  }

  public currancyFromHistory(uuidCurrency: string): CurrencyConverter {
    const histories: CurrencyConverter[] = this.getHistories();
    return histories.find((item: CurrencyConverter) => item.uuid === uuidCurrency );
  }

  public addHistory(newCurrency: CurrencyConverter): void {
    const histories: CurrencyConverter[] = this.getHistories();
    histories.push(newCurrency);
    localStorage.setItem(this.localStorageHistory, JSON.stringify(histories));
  }

  public deleteCurrencyFromHistory(uuidCurrency: string): void {
    let histories: CurrencyConverter[] = this.getHistories();
    histories = histories.filter((currency: CurrencyConverter) => currency.uuid !== uuidCurrency);
    localStorage.setItem(this.localStorageHistory, JSON.stringify(histories));
  }

  public sortByCreatedAt(histories: CurrencyConverter[]): CurrencyConverter[] {
    return histories.sort((item1: CurrencyConverter, item2: CurrencyConverter) =>  +new Date(item2.createdAt) - +new Date(item1.createdAt));
  }
}
