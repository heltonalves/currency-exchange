import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { CurrencyService } from './currency.service';
import { CurrencyRate } from '@core/models';

describe('CurrencyService', () => {
  let service: CurrencyService;
  let httpTestingController: HttpTestingController;

  let store = {};
  const mockLocalStorage = {
    getItem: (key: string): string => {
      return key in store ? store[key] : null;
    },
    setItem: (key: string, value: string) => {
      store[key] = `${value}`;
    },
  };

  const mockCurrencyRate: CurrencyRate[] = [
    { currency: 'currency1', rate: 1.000 },
    { currency: 'currency1', rate: 1.000 },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ HttpClient ],
    });

    service = TestBed.get(CurrencyService);
    httpTestingController = TestBed.get(HttpTestingController);

    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);

    spyOn(service, 'currenciesRatesInUSD')
      .and.returnValue(of(mockCurrencyRate));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load and return currency rate list from localStorage', () => {
    mockLocalStorage.setItem(service.localStorageKey, JSON.stringify(mockCurrencyRate));

    service.currenciesRate().subscribe(currencies => {
      expect(currencies.length).toBe(2);
      expect(currencies).toEqual(mockCurrencyRate);
    });
  });

  it('should return currency rate list from currenciesRatesInUSD', () => {
    service.currenciesRate().subscribe(currencies => {
      expect(currencies.length).toBe(2);
      expect(currencies).toEqual(mockCurrencyRate);
      expect(service.currenciesRatesInUSD).toHaveBeenCalled();
    });
  });

  afterEach(() => {
    httpTestingController.verify();
    store = {};
  });
});
