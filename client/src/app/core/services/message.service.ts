import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { Message } from '@core/models';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private subject = new BehaviorSubject<Message|any>('');

  constructor() { }

  private statusHandler (type: string, message: string): void {
    return this.subject.next({ type: type, text: message });
  }

  public success(message: string): void {
    this.statusHandler('success', message);
  }

  public error(message: string): void {
    this.statusHandler('error', message);
  }

  public getMessage(): Observable<any> { return this.subject.asObservable(); }
}
