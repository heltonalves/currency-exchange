import {
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { httpInterceptorProviders } from './interceptors';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { CurrencyService } from './services/currency.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    AuthService,
    UserService,
    CurrencyService,
    httpInterceptorProviders,
  ],
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
