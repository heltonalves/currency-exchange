export interface User {
  username: string;
  password: string;
  fullName: string;
}

export interface CurrentUser {
  user: User;
  token: string;
}
