export interface CurrencyRate {
  currency: string;
  rate: number;
}

export interface CurrencyConverter {
  amount?: number;
  total?: number;
  from?: CurrencyRate;
  to?: CurrencyRate;
  createdAt?: string;
  uuid?: string;
}

export interface ExchangeCurrency {
  rate?: string;
  timestamp?: string;
}

export interface CurrencyStatistic {
  lowest?: number;
  highest?: number;
  average?: number;
}
