import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpInterceptor,
} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import * as users from 'data/users';

@Injectable()
export class FakeUserInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // request = request.clone({
    //   setHeaders: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    // });

    if (request.url.endsWith('/users') && request.method === 'GET') {
      return of(new HttpResponse({ status: 200, body: users.default }));
    }

    return next.handle(request);
  }
}
