import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HeadersInterceptor } from './auth-interceptor';
import { FakeUserInterceptor } from './fake-user-interceptor';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: FakeUserInterceptor, multi: true },
];
