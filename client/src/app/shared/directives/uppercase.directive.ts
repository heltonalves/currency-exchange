import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appUppercase]',
})
export class UppercaseDirective {

  @HostListener('keyup', [ '$event.target' ])
  checkInput(target: HTMLInputElement): void {
    if (target.value) {
      target.value = target.value.toUpperCase();
    }
  }
}
