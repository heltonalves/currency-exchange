import { Component } from '@angular/core';

import { CurrencyService } from '@core/services';
import { CurrencyConverter } from '@core/models';

@Component({
  selector: 'app-conversion-history',
  templateUrl: './conversion-history.component.html',
  styleUrls: ['./conversion-history.component.scss'],
})
export class ConversionHistoryComponent {
  displayedColumns: string[] = ['date', 'event', 'actions'];
  histories: CurrencyConverter[];

  constructor(
    private currencyService: CurrencyService,
  ) {
    this.loadHistories();
  }

  public loadHistories(): void {
    this.histories = this.currencyService.getHistories();
  }

  public deleteCurrency(uuidCurrency: string): void {
    this.currencyService.deleteCurrencyFromHistory(uuidCurrency);
    this.loadHistories();
  }

}
