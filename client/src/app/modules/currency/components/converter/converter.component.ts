import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as uuid from 'uuid';

import { CurrencyService } from '@core/services';
import { CurrencyConverter, CurrencyRate } from '@core/models';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.scss'],
})
export class ConverterComponent implements OnInit {
  converterForm: FormGroup;
  currencyConverted: CurrencyConverter = {};

  constructor(
    private formBuilder: FormBuilder,
    private currencyService: CurrencyService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        const uuidCurrency = params['uuid'];
        this.openCurrency(uuidCurrency);
      });
  }

  public openCurrency(uuidCurrency: string): void {
    this.currencyConverted = this.currencyService.currancyFromHistory(uuidCurrency) || {};
    this.createCurrencyForm();
  }

  public clearParams(): void {
    this.router.navigate([], {
      queryParams: {
        uuid: null,
      },
      queryParamsHandling: 'merge',
    });
  }

  public createCurrencyForm(): void {
    const amount = this.currencyConverted.amount || '';
    const from = (this.currencyConverted.from) ? this.currencyConverted.from.currency : '';
    const to = (this.currencyConverted.to) ? this.currencyConverted.to.currency : '';

    this.converterForm = this.formBuilder.group({
      amount: [amount, Validators.required],
      from: [from, Validators.required],
      to: [to, Validators.required],
    });
  }

  public toggleCurrency(): void {
    const [fromValue, toValue] = this.getFromTo();
    this.converterForm.get('from').setValue(toValue);
    this.converterForm.get('to').setValue(fromValue);
  }

  public getFromTo(): string[] {
    return [
      this.converterForm.get('from').value.toUpperCase(),
      this.converterForm.get('to').value.toUpperCase(),
    ];
  }

  public checkCurrency(): void {
    this.setInvalidCurrency('from');
    this.setInvalidCurrency('to');
  }

  public setInvalidCurrency(currency: string) {
    const value: string = this.converterForm.get(currency).value.toUpperCase();

    if (value && !this.currencyService.getCurrencyRate(value)) {
      this.converterForm.controls[currency].setErrors({'incorrect': true});
    }
  }

  public converter(): void {
    if (this.converterForm.invalid) { return; }

    this.buildConvertedCurrency();
    this.router.navigate(['/currency/converter/', this.currencyConverted.uuid]);
  }

  public buildConvertedCurrency(): void {
    const [fromValue, toValue] = this.getFromTo();
    this.currencyConverted.amount = this.converterForm.get('amount').value;
    this.currencyConverted.from = this.currencyService.getCurrencyRate(fromValue);
    this.currencyConverted.to = this.currencyService.getCurrencyRate(toValue);

    this.calcCurrency();
  }

  public calcCurrency() {
    const amount: number = this.currencyConverted.amount;
    const from: CurrencyRate = this.currencyConverted.from;
    const to: CurrencyRate = this.currencyConverted.to;

    this.currencyConverted.total = (amount * from.rate) / to.rate;
    this.currencyConverted.createdAt = new Date().toLocaleString();
    this.currencyConverted.uuid = uuid.v4();

    this.currencyService.addHistory(this.currencyConverted);
  }
}
