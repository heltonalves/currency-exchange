import {
  CurrencyConverter,
  ExchangeCurrency,
  CurrencyStatistic,
} from '@client/core/models';

import { Component, Input } from '@angular/core';

import { CurrencyService } from '@client/core/services';

@Component({
  selector: 'app-exchange-history',
  templateUrl: './exchange-history.component.html',
  styleUrls: ['./exchange-history.component.scss'],
})
export class ExchangeHistoryComponent {
  displayedColumnsExchange: string[] = ['date', 'exchange-rate'];
  displayedColumnsStatistics: string[] = ['statistics', 'rate'];
  listDays = [
    { value: 7, viewValue: '7 days' },
    { value: 14, viewValue: '14 days' },
    { value: 30, viewValue: '30 days' },
  ];
  selectedDay = 7;
  currency: CurrencyConverter;

  listHistory: ExchangeCurrency[] = [];
  listStatistic = [];

  @Input()
  set currentCurrency(currency: CurrencyConverter) {
    this.currency = currency;
    this.getExchangeHistory();
  }

  constructor(
    private currencyService: CurrencyService,
  ) { }

  public getExchangeHistory(): void {
    this.currencyService.exchangeRatesHistory(this.currency.from.currency, this.selectedDay)
      .subscribe(
        (resp: ExchangeCurrency[]) => {
          this.listHistory = resp;
          this.calcStatistics();
        },
        error => {
          console.log(error, 'error in exchangeRatesHistory.');
        });
  }

  public calcStatistics(): void {
    const average = this.listHistory.reduce((total, item) => {
      return total + parseFloat(item.rate);
    }, 0) / this.listHistory.length;

    const highest = Math.max.apply(Math, this.listHistory.map(function(item) { return parseFloat(item.rate); }));
    const lowest = Math.min.apply(Math, this.listHistory.map(function(item) { return parseFloat(item.rate); }));

    this.listStatistic = [
      { name: 'Average', value: average },
      { name: 'Lowest', value: lowest },
      { name: 'Highest', value: highest },
    ];
  }
}
