import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConverterComponent } from './components';
import { ConversionHistoryComponent } from './components';

const routes: Routes = [
  {
    path: 'converter/:uuid',
    component: ConverterComponent,
  },
  {
    path: 'converter',
    component: ConverterComponent,
  },
  {
    path: 'conversion-history',
    component: ConversionHistoryComponent,
  },
  {
    path: '**',
    redirectTo: 'converter',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrencyRoutingModule { }
