import { NgModule } from '@angular/core';
import { SharedModule } from '@client/shared/shared.module';

import { CurrencyRoutingModule } from './currency-routing.module';
import { ConverterComponent } from './components/converter/converter.component';
import { ConversionHistoryComponent } from './components/conversion-history/conversion-history.component';
import { ExchangeHistoryComponent } from './components/exchange-history/exchange-history.component';

@NgModule({
  imports: [
    SharedModule,
    CurrencyRoutingModule,
  ],
  declarations: [
    ConverterComponent,
    ConversionHistoryComponent,
    ExchangeHistoryComponent,
  ],
})
export class CurrencyModule { }
