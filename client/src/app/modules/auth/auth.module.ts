import { NgModule } from '@angular/core';
import { SharedModule } from '@client/shared/shared.module';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './components';

@NgModule({
  imports: [
    SharedModule,
    AuthRoutingModule,
  ],
  declarations: [ LoginComponent ],
})
export class AuthModule { }
