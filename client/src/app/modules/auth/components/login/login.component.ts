import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService, MessageService } from '@core/services';
import { User } from '@core/models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public onSubmit(): void {
    if (this.loginForm.invalid) { return; }

    this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
      .subscribe(
        (user: User) => {
          if (user) {
            this.router.navigate(['/']);
            return;
          }

          this.messageService.error('The username or password is incorrect, please input again.');
        },
        error => {
          this.messageService.error('Error occurred during Login. Please contact your system administrator.');
          console.log(error, 'Error in login');
        },
      );
  }
}
