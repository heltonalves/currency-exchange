import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@client/shared/shared.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';
import { HeaderComponent, MessageComponent } from '@core/components';

import { CurrencyService } from '@core/services';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let currencyServiceStub: Partial<CurrencyService>;

  currencyServiceStub = jasmine.createSpyObj('CurrencyService', ['loadCurrencies']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        SharedModule,
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        MessageComponent,
      ],
      providers: [
        { provide: CurrencyService, useValue: currencyServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadCurrencies', () => {
    expect(currencyServiceStub.loadCurrencies).toHaveBeenCalled();
  });

});
