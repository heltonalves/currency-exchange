export default [
  {
    username: 'user1',
    password: 'pass1',
    fullName: 'John Do',
  },
  {
    username: 'user2',
    password: 'pass2',
    fullName: 'Adam Smit',
  },
];
