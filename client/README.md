# Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

## Development server

Install dependencies: `npm install`<br/>
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Build

Run `npm rum build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
